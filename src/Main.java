import java.util.Scanner;
public class Main {

    public static void main(String[] args) throws java.io.IOException{
        Scanner _scan = new Scanner (System.in);
        System.out.printf("Enter ambient temperature: ");
        double t = _scan.nextDouble();
        System.out.printf("It is raining outside? y/n: ");
        char rain = (char) System.in.read ();
        System.out.println("Enter wind speed in m/s: ");
        double speed = _scan.nextDouble();

        if(rain == 'n' && t>=15 && speed<=3){
            System.out.println("Today you can walk");
        }
        else{
            System.out.println("\n" + "Don't walk today");
        }



    }
}